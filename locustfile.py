from locust import HttpUser, task, between

class PlanhubWebUser(HttpUser):
    wait_time = between(1, 2.5)

    @task
    def homepage(self):
        self.client.get("/", name="homepage")