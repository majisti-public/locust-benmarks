Read our analysis on https://docs.planhub.k8s.majisti.com

# Usage

`make`

If needed, modify `VIRTUAL_HOST` by creating the `.env.local` file

By default, you can visit `http://benchmarks.local.planhub.ca`
