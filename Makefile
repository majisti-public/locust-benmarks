include ./config/make/variables.mk

.PHONY: install
default: build-images start

build-images:
	bin/docker-compose build

## Displays running container logs
logs:
	bin/docker-compose logs --tail 200 -f

restart: stop start
stop:
	bin/docker-compose kill
	bin/docker-compose down --remove-orphans

start:
	bin/docker-compose up -d master worker
	bin/docker-compose up -d --scale worker=7

## Restart the development server
restart: stop start

include ./config/make/help-target.mk
include ./config/make/nginx-proxy-target.mk
