## Destroys and recreates the external nginx-proxy container.
nginx-proxy-init: nginx-proxy-destroy nginx-proxy-create

## Create the external nginx-proxy container.
nginx-proxy-create: nginx-proxy-network-create
	docker run \
		--network=nginx-proxy \
		--name=nginx-proxy \
		-d -p 80:80 \
		-d -p 443:443 \
		--restart=always \
		--volume /var/run/docker.sock:/tmp/docker.sock:ro \
		--volume ~/.majisti/local-dev/certs:/etc/nginx/certs \
		jwilder/nginx-proxy || true

## Create the external nginx-proxy container.
nginx-proxy-network-create:
	docker network create nginx-proxy || true

## Destroy the external nginx-proxy container.
nginx-proxy-destroy: stop
	docker stop nginx-proxy || true && docker rm nginx-proxy || true
	docker network rm nginx-proxy || true && docker network create nginx-proxy || true
